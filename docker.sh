#!/bin/bash

docker stop TIMWI
docker rm TIMWI

docker build -t timwi:latest .

docker create -it -v $1:/docker -p 8080:8080 -p 3000:3000 --name TIMWI timwi:latest
docker start TIMWI
docker exec -it TIMWI bash