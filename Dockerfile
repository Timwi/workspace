FROM ubuntu:16.04

RUN apt update
RUN apt install -y curl

# Installation de node
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt install -y nodejs

# RUN apt install -y npm

CMD ["/bin/bash"]