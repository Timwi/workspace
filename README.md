Pour faire fonctionner le projet, cloné ce repo (workspace) puis le front et le back dans workspace de façon à avoir :

 - workspace/
    - Frontend/
    - Backend/
    - Dockerfile
    - Docker.sh
    
    
Une fois cela fait, lancé le script docker.sh avec comme paramètre le path absolu vers le dossier workspace : /home/xxxx/xxx/workspace

Une fois le script exécuter, vous devriez vous trouver dans le container docker, il ne reste plu que à lancer le front et le back:

npm install --prefix docker/workspace/backend && node docker/workspace/backend/api/app.js &
npm install --prefix docker/workspace/frontend && npm run dev --prefix docker/workspace/frontend

Il est maintenant possible d'accéder à l'application sur le port 8080.
